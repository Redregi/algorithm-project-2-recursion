Short Comparison Recursive vs Iterative
Recursive takes more code and slows down the program due to large stack list, iterative is more simple and is done faster. Conclusion: I like iterative better 